from decimal import Decimal

from django.test import TestCase

from currency.api import convert
from currency.models import Currency, Rate


class Test(TestCase):

    def setUp(self):
        eur = Currency.objects.get_or_create(code='EUR', name='Euro')[0]
        czk = Currency.objects.get_or_create(code='CZK',
                                             name='Czech Republic Koruna')[0]
        pln = Currency.objects.get_or_create(code='PLN',
                                             name='Polish Zloty')[0]

        Rate.objects.create(currency=eur, rate=Decimal('0.85'))
        Rate.objects.create(currency=czk, rate=Decimal('22.5'))
        Rate.objects.create(currency=pln, rate=Decimal('3.7'))

    def test_convert(self):
        # 10 EUR = 11.7647 USD
        self.assertEqual(convert(Decimal('10'), 'EUR', 'USD', 2),
                         Decimal('11.76'))
        self.assertEqual(convert(Decimal('10'), 'EUR', 'USD', 3),
                         Decimal('11.765'))

    def test_reverse_convert(self):
        # 10 USD = 8.5 EUR
        self.assertEqual(convert(Decimal('10'), 'USD', 'EUR', 2),
                         Decimal('8.5'))

    def test_cross_convert(self):
        # 10 EUR = 11.765 USD = 264.706 CZK
        self.assertEqual(convert(Decimal('10'), 'EUR', 'CZK', 2),
                         Decimal('264.71'))

        # 10 PLN = 2.703 USD = 60.811 CZK
        self.assertEqual(convert(Decimal('10'), 'PLN', 'CZK', 2),
                         Decimal('60.81'))

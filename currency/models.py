from django.conf import settings
from django.db import models


class UnsupportedCurrency(Exception):
    def __init__(self, currency):
        self.currency = currency

    def __str__(self):
        return 'Unsupported currency {}'.format(self.currency)


class Currency(models.Model):
    code = models.CharField(
        max_length=3, unique=True,
        choices=[(c, c) for c in settings.VALID_CURRINCIES])
    name = models.CharField(max_length=64)

    class Meta:
        verbose_name = 'currency'
        verbose_name_plural = 'currencies'

    def __str__(self):
        return self.code


class RateManager(models.Manager):

    def rate(self, currency):
        # return last rate for currency
        try:
            return self.filter(currency__code=currency).latest('datetime').rate
        except Rate.DoesNotExist:
            raise UnsupportedCurrency(currency)


class Rate(models.Model):
    """exchange rate against the dollar"""
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE)
    rate = models.DecimalField(max_digits=10, decimal_places=3)
    datetime = models.DateTimeField(auto_now_add=True)

    objects = RateManager()

    class Meta:
        verbose_name = 'rate'
        verbose_name_plural = 'rates'
        ordering = '-datetime',

    def __str__(self):
        return '1.000 USD = {} {}'.format(self.rate, self.currency)

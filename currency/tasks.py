from __future__ import absolute_import, unicode_literals
from celery import task

from currency.api import update_rates


@task
def update_rates_on_schedule():
    update_rates()

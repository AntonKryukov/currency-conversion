from django.contrib import admin

# Register your models here.
from currency.models import Currency, Rate


@admin.register(Currency)
class CurrencyAdmin(admin.ModelAdmin):
    list_display = 'code', 'name'


@admin.register(Rate)
class RateAdmin(admin.ModelAdmin):
    list_display = '__str__', 'datetime'
    list_filter = 'currency', 'datetime'

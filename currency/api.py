import requests
from django.conf import settings

from currency.models import Rate, Currency


def convert(value, source, target, decimal_places=3):

    # 1. convert any value to USD
    if source != 'USD':
        rate = Rate.objects.rate(source)
        usd = value / rate
    else:
        usd = value

    if target == 'USD':
        return round(usd, decimal_places)

    # 2. convert USD to target
    rate = Rate.objects.rate(target)
    return round(usd * rate, decimal_places)


def update_rates():
    url = 'https://openexchangerates.org/api/latest.json?app_id={}'.format(
        settings.OPENEXCHANGERATES_API_KEY)

    r = requests.get(url)
    rates = r.json()['rates']
    for currency in Currency.objects.all():
        Rate.objects.create(currency=currency, rate=rates[currency.code])

from django.core.management.base import BaseCommand

from currency.api import update_rates


class Command(BaseCommand):

    def handle(self, *args, **options):
        update_rates()

# currency_convertion


### Локальная инсталяция проекта:

1. Установить virtualenv, выполнить pip install -r requirements.pip
2. Создать бд и добавить local_settings.py рядом с currency_conversion/settings/local_settings.py.example
3. Ключ OPENEXCHANGERATES_API_KEY в local_settings.py можно скопировать из local_settings.py.example
4. ./manage.py migrate
5. Для обновления курсов валют: ./manage.py update_rates
6. Для автоматического обновления валют каждый час: celery -A currency_conversion worker -B -l info
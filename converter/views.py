from django.views.generic import TemplateView
from currency.models import Currency


class ConverterPageView(TemplateView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['currencies'] = Currency.objects.all()
        return context

from django.conf import settings
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from converter.serializers import ConverterSerializer
from currency.api import convert
from currency.models import UnsupportedCurrency


class ConverterView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, format=None):

        serializer = ConverterSerializer(data=request.data)
        if serializer.is_valid():
            source = serializer.validated_data.get('source')
            target = serializer.validated_data.get('target')
            value = serializer.validated_data.get('value')
            try:
                converted = convert(value, source, target,
                                    settings.DECIMAL_PLACES)
            except UnsupportedCurrency as e:
                return Response({
                    'error': str(e)
                }, status=status.HTTP_400_BAD_REQUEST)

            return Response({
                'converted': converted
            })
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

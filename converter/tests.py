from decimal import Decimal

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from currency.models import Currency, Rate


class TestConverter(APITestCase):

    def setUp(self):
        eur = Currency.objects.get_or_create(code='EUR', name='Euro')[0]
        czk = Currency.objects.get_or_create(code='CZK',
                                             name='Czech Republic Koruna')[0]
        pln = Currency.objects.get_or_create(code='PLN',
                                             name='Polish Zloty')[0]

        Rate.objects.create(currency=eur, rate=Decimal('0.85'))
        Rate.objects.create(currency=czk, rate=Decimal('22.5'))
        Rate.objects.create(currency=pln, rate=Decimal('3.7'))

        self.converter_url = reverse('converter')

    def test_valid_conversation(self):
        data = {
            "source": "USD",
            "value": 10,
            "target": "EUR"
        }

        response = self.client.post(self.converter_url, data, format='json')
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(response.data.get('converted'), Decimal('8.5'))

    def test_invalid_conversation(self):
        data = {
            "source": "USDU",
            "value": 10,
            "target": "EUR"
        }

        response = self.client.post(self.converter_url, data, format='json')
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)

    def test_invalid_value(self):
        data = {
            "source": "USD",
            "value": "val",
            "target": "EUR"
        }

        response = self.client.post(self.converter_url, data, format='json')
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)

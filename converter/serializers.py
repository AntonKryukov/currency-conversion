from django.conf import settings
from rest_framework import serializers


def available_currency(value):
    if value not in settings.VALID_CURRINCIES:
        raise serializers.ValidationError(
            'This field must be a valid currency')


class ConverterSerializer(serializers.Serializer):
    value = serializers.DecimalField(max_digits=10, decimal_places=3)
    source = serializers.CharField(validators=[available_currency])
    target = serializers.CharField(validators=[available_currency])

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass
